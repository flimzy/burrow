package burrow

import (
	"fmt"
	"log/slog"
	"regexp"
)

// Option is a functional that can be used to configure a Burrow instance or source.
type Option interface {
	apply(any) error
}

type exclude string

// Exclude returns an Option that excludes migrations matching the given regular
// expression pattern.
func Exclude(pattern string) Option {
	return exclude(pattern)
}

func (e exclude) apply(t any) error {
	if s, ok := t.(*fsSource); ok {
		re, err := regexp.Compile(string(e))
		if err != nil {
			return err
		}
		s.excludes = append(s.excludes, re)
		return nil
	}
	return fmt.Errorf("cannot apply Exclude option to %T", t)
}

type dialect int

var _ Option = dialect(0)

func (f dialect) apply(t any) error {
	if m, ok := t.(*Migrator); ok {
		m.dbDialect = f
		return nil
	}
	return fmt.Errorf("cannot apply Flavor option to %T", t)
}

// Supported SQL Dialects. Pass one of these as an argument to [New] to enable
// dialect-specific features.
const (
	// DialctNone is the default, and indicates that burrow will attempt to
	// detect the dialect automatically.
	DialectNone dialect = iota
	// DialectMySQL enables the use of GET_LOCK() and RELEASE_LOCK() in addition
	// to row-level locking on the migrations table, since MySQL doesn't
	// properly support normal transactions for table creation and alternation
	// queries.
	DialectMySQL
	// DialectPostgres enables Postgres-specific features.
	DialectPostgres
	// DialectPgx enables Postgres-specific features using the pgx driver, with
	// COPY support.
	DialectPgx
	// DialectSQLite enables SQLite-specific features.
	DialectSQLite
)

type withLogger struct {
	logger *slog.Logger
}

func (w withLogger) apply(t any) error {
	if m, ok := t.(*Migrator); ok {
		m.logger = w.logger
		return nil
	}
	return fmt.Errorf("cannot apply WithLogger option to %T", t)
}

// WithLogger allows setting a custom logger for logs.
//
// A single INFO-level log is emitted for each migration, indicating whether
// it was run, or skipped.  A failed migration logs an error, as well as
// returning the error value. All other information is logged at DEBUG level.
func WithLogger(logger *slog.Logger) Option {
	return withLogger{logger}
}

type withStateTable string

func (w withStateTable) apply(t any) error {
	if m, ok := t.(*Migrator); ok {
		m.stateTable = string(w)
		return nil
	}
	return fmt.Errorf("cannot apply WithStateTable option to %T", t)
}

// WithStateTable allows specifying a custom state table name.
func WithStateTable(table string) Option {
	return withStateTable(table)
}

type executable string

func (e executable) apply(t any) error {
	if s, ok := t.(*fsSource); ok {
		re, err := regexp.Compile(string(e))
		if err != nil {
			return err
		}
		s.executable = append(s.executable, re)
		return nil
	}
	return fmt.Errorf("cannot apply Exclude option to %T", t)
}

// Executable returns an Option that matches files which should be executed
// directly, rather than interpreted as SQL.
//
// Example:
//
//	source, err := FileSource("migrations", Executable(`\.sh$`))
//
// Binaries executed this way will have certain environment variables set,
// depending on the database driver in use.
//
// For SQLite:
//
//   - SQLITE_FILE is set to the path of the database file. (empty for in-memory
//     databases)
func Executable(pattern string) Option {
	return executable(pattern)
}
