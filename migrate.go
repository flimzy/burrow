package burrow

import (
	"context"
	"database/sql"
	"database/sql/driver"
	"errors"
	"fmt"
	"io"
	"log/slog"
	"reflect"
	"slices"

	"github.com/go-sql-driver/mysql"
)

// DefaultStateTable is the default name of the table where migration state is
// stored. It is created the first time [Migrator.Migrate] is called.
const DefaultStateTable = "migrations"

// Migrator is an instance that applies migrations to a database. A Migrator
// instance is stateful, and should only be used once after initialization. If
// you need to apply the same migration to multiple databases, or to the same
// database multiple times, create one Migrator instance for migration run.
type Migrator struct {
	db         *sql.DB
	stateTable string
	minVer     uint
	sources    []Source
	dbDialect  dialect
	logger     *slog.Logger
}

// New returns a new Migrator that will apply changes to db.
func New(db *sql.DB, options ...Option) (*Migrator, error) {
	m := &Migrator{
		db:         db,
		stateTable: DefaultStateTable,
		logger:     slog.Default(),
	}
	for _, opt := range options {
		if err := opt.apply(m); err != nil {
			return nil, err
		}
	}
	if m.dbDialect == DialectNone {
		m.dbDialect = detectDialect(db.Driver())
	}
	return m, nil
}

// detectDialect detects the dialect of the database driver. It uses reflection
// so as not to bloat the import list with countless database drivers.
func detectDialect(db driver.Driver) dialect {
	rv := reflect.ValueOf(db).Elem()

	switch rv.Type().PkgPath() {
	case "github.com/mattn/go-sqlite3":
		return DialectSQLite
	case "github.com/go-sql-driver/mysql":
		return DialectMySQL
	case "github.com/jackc/pgx/v5/stdlib":
		return DialectPgx
	}
	return DialectNone
}

// AddSource adds a  source to m. You may provide multiple sources, but the
// migration IDs returned must be unique across all sources.
func (m *Migrator) AddSource(s Source) {
	m.sources = append(m.sources, s)
}

// ValidateSources does minimal validation of the sources. Notably, it checks
// that the migration IDs are unique across all sources. There is normally no
// need to call this function, as [Migrator.Migrate] does the same checks, but
// it can be useful to do these checks separataely, such as in a build pipeline
// test.
func ValidateSources(ctx context.Context, source ...Source) error {
	m := &Migrator{
		sources: source,
		logger:  slog.New(slog.NewTextHandler(io.Discard, nil)),
	}
	_, _, err := m.readSources(ctx)
	return err
}

// SetMinVer sets the minimum version to apply. It is meant to be used when
// adding migrations to an existing system, to avoid applying migrations that
// were already applied manually.
func (m *Migrator) SetMinVer(ver uint) {
	m.minVer = ver
}

type indexEntry struct {
	source Source
	mig    *Migration
	err    error
	// ready will be closed when the PreRun function, if any, has completed.
	ready <-chan error
}

// Migrate performs the actual migration. It returns the number of successful
// migration scripts applied.
//
// Migrate will create the state table if it does not exist. It will also lock
// the state table for each migration. If Migrate returns a non-nil error, it
// means that all migrations up to that point were applied successfully.
func (m *Migrator) Migrate(ctx context.Context) (int, error) {
	if err := m.initStateTable(ctx); err != nil {
		return 0, err
	}
	curVer, err := m.currentVersion(ctx)
	if err != nil {
		return 0, err
	}

	migrations, preRun, err := m.readSources(ctx)
	if err != nil {
		return 0, err
	}

	startLog := m.logger
	var nextVer uint
	if curVer != nil {
		startLog = startLog.With(slog.Int("current_schema_version", int(*curVer)))
		nextVer = *curVer + 1
	}
	if m.minVer > nextVer {
		nextVer = m.minVer
	}
	startLog = startLog.With(slog.Int("next_version", int(nextVer)))
	index := make([]uint, 0, len(migrations))
	for id := range migrations {
		if id >= nextVer {
			index = append(index, id)
		}
	}
	startLog.With(slog.Int("migrations_to_apply", len(index))).Info("Running migrations")
	if len(index) == 0 {
		return 0, nil
	}
	slices.Sort(index)

	// Kick off any PreRun functions
	for _, ver := range index {
		fn := preRun[ver]
		if fn == nil {
			continue
		}

		ch := make(chan error)
		mig := migrations[ver]
		mig.ready = ch
		go func() {
			ch <- fn(ctx)
			close(ch)
		}()
	}

	var applied int
	for _, ver := range index {
		entry, ok := migrations[ver]
		if !ok {
			m.logger.Info("migration not found; skipping", slog.Int("version", int(ver)))
		}
		mig, err := entry.mig, entry.err
		if err != nil {
			return applied, err
		}
		if entry.ready != nil {
			if err := <-entry.ready; err != nil {
				m.logger.Error("prerun failed", slog.Int("version", int(ver)), slog.Any("error", err))
				return applied, err
			}
		}
		if err := m.applyMigration(ctx, mig); err != nil {
			if errors.Is(err, errConcurrentMigration) {
				m.logger.Info("migration locked by concurrent migration; skipping", slog.Int("version", int(ver)))
				continue
			}

			m.logger.Error("migration failed", slog.Int("version", int(ver)), slog.Any("error", err))

			return applied, err
		}
		m.logger.Info("migration applied successfully", slog.Int("version", int(ver)))
		applied++
	}

	return applied, nil
}

func (m *Migrator) readSources(ctx context.Context) (map[uint]*indexEntry, map[uint]func(context.Context) error, error) {
	migrations := map[uint]*indexEntry{}
	preRun := map[uint]func(context.Context) error{}
	for sourceIdx, s := range m.sources {
		if sd, ok := s.(sourceParticulars); ok {
			if err := sd.setParticulars(ctx, &particulars{
				dialect: m.dbDialect,
				db:      m.db,
			}); err != nil {
				return nil, nil, err
			}
		}
		index, err := s.Index(ctx)
		if err != nil {
			return nil, nil, err
		}
		m.logger.Debug("found migrations", slog.Int("source_id", sourceIdx+1), slog.Int("count", len(index)))
		for _, id := range index {
			if _, ok := migrations[id]; ok {
				return nil, nil, fmt.Errorf("migration %d defined in multiple sources", id)
			}
			// If there's an error, we delay reporting it until we've attempted
			// all preceding migrations.
			mig, err := s.Migration(ctx, id)
			preRun[id] = mig.PreRun
			migrations[id] = &indexEntry{
				source: s,
				mig:    mig,
				err:    err,
			}
		}
	}
	return migrations, preRun, nil
}

func (m *Migrator) applyMigration(ctx context.Context, mig *Migration) error {
	conn, err := m.db.Conn(ctx)
	if err != nil {
		return err
	}
	defer conn.Close()

	defaultApply := func(ctx context.Context, tx *sql.Tx) error {
		body, err := mig.body(ctx)
		if err != nil {
			return err
		}
		defer body.Close()

		query, err := io.ReadAll(body)
		if err != nil {
			return err
		}
		_, err = tx.ExecContext(ctx, string(query))
		return err
	}
	var apply func(context.Context, *sql.Tx) error

	switch {
	case mig.Run != nil:
		apply = func(ctx context.Context, tx *sql.Tx) error {
			switch err := mig.Run(ctx, tx); err {
			case nil:
				return nil
			case ErrUnimplemented:
				// Continue with the default apply function.
			default:
				return err
			}
			return defaultApply(ctx, tx)
		}
	case m.dbDialect == DialectPgx:
		return m.applyMigrationPgx(ctx, conn, mig)
	default:
		apply = defaultApply
	}

	tx, err := conn.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	if err := m.lock(ctx, conn, mig.Version); err != nil {
		return err
	}
	defer func() {
		if err := m.unlock(ctx, conn); err != nil {
			m.logger.Error("failed to release lock", slog.Any("error", err))
		}
	}()
	if err := apply(ctx, tx); err != nil {
		if rerr := m.rollback(ctx, tx, mig.Version); rerr != nil {
			return errors.Join(err, fmt.Errorf("failed to rollback migration %d: %w", mig.Version, rerr))
		}
		return err
	}
	return tx.Commit()
}

func (m *Migrator) rollback(ctx context.Context, tx *sql.Tx, id uint) error {
	if m.dbDialect != DialectMySQL {
		return nil
	}
	_, err := tx.ExecContext(ctx, `DELETE FROM `+m.stateTable+` WHERE version = ?`, id)
	return err
}

// lock locks the state table for writing by adding the migration within a
// transaction. It returns an error if the lock cannot be acquired, or if the
// migration with ID id has already been applied.
func (m *Migrator) lock(ctx context.Context, conn *sql.Conn, id uint) error {
	if m.dbDialect == DialectMySQL {
		// MySQL doesn't support "proper" transactions, so we need to do an extra
		// locking layer.
		var locked sql.NullInt64
		if err := conn.QueryRowContext(ctx, `SELECT GET_LOCK('burrow_migration', -1)`).Scan(&locked); err != nil {
			return err
		}
		if locked.Int64 == 0 {
			return errConcurrentMigration
		}
	}
	query := `INSERT INTO ` + m.stateTable + ` (version) VALUES (?)`
	switch m.dbDialect {
	case DialectPostgres, DialectPgx:
		query = `INSERT INTO ` + m.stateTable + ` (version) VALUES ($1)`
	}
	_, err := conn.ExecContext(ctx, query, id)
	if err != nil {
		_ = m.unlock(ctx, conn)
		return decorateConcurrentMigrationError(err)
	}

	return nil
}

func (m *Migrator) unlock(ctx context.Context, conn *sql.Conn) error {
	if m.dbDialect != DialectMySQL {
		return nil
	}
	var unlocked sql.NullInt64
	if err := conn.QueryRowContext(ctx, `SELECT RELEASE_LOCK('burrow_migration')`).Scan(&unlocked); err != nil {
		return err
	}
	if unlocked.Int64 == 0 {
		return errors.New("failed to release lock")
	}

	return nil
}

type customError string

func (e customError) Error() string {
	return string(e)
}

// errConcurrentMigration is returned by Migrate when the state table is locked
// by another concurrent migration.
const errConcurrentMigration = customError("state table locked by concurrent migration")

// ErrUnimplemented may be returned by [Migration.PreRun], [Migration.Run], or
// [Migration.GetBody] to indicate that the respective method is not implemented,
// and that the migration should fall back to other methods.
const ErrUnimplemented = customError("unimplemented")

// decorateConcurrentMigrationError converts err to ErrConcurrentMigration if
// it is an error that indicates a concurrent migration.
//
// It only understands MySQL and SQLite3 errors at the moment.
func decorateConcurrentMigrationError(err error) error {
	mysqlErr := new(mysql.MySQLError)
	if errors.As(err, &mysqlErr) {
		if mysqlErr.Number == 1062 {
			return errConcurrentMigration
		}
		return err
	}
	if isSQLite3MigrationError(err) {
		return errConcurrentMigration
	}
	return err
}

func (m *Migrator) initStateTable(ctx context.Context) error {
	switch m.dbDialect {
	case DialectPostgres, DialectPgx:
		_, err := m.db.ExecContext(ctx, `CREATE TABLE IF NOT EXISTS `+m.stateTable+` (
			version INTEGER PRIMARY KEY,
			applied_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP
		)`)
		return err

	default:
		_, err := m.db.ExecContext(ctx, `CREATE TABLE IF NOT EXISTS `+m.stateTable+` (
			version INTEGER PRIMARY KEY,
			applied_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
		)`)
		return err
	}
}

func (m *Migrator) currentVersion(ctx context.Context) (*uint, error) {
	var version sql.NullInt64
	err := m.db.QueryRowContext(ctx, `SELECT MAX(version) FROM `+m.stateTable).Scan(&version)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}
	if !version.Valid {
		return nil, nil
	}
	ver := uint(version.Int64)
	return &ver, err
}
