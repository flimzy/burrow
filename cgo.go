//go:build cgo
// +build cgo

package burrow

import (
	"errors"

	"github.com/mattn/go-sqlite3"
)

func isSQLite3MigrationError(err error) bool {
	var sqliteErr sqlite3.Error
	if errors.As(err, &sqliteErr) {
		return sqliteErr.Code == sqlite3.ErrConstraint
	}
	return false
}
