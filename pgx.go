package burrow

import (
	"bufio"
	"bytes"
	"context"
	"database/sql"
	"fmt"
	"regexp"
	"strings"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/stdlib"
)

// e.g. COPY "foo".bar (zip, zipname, city, county, area, region, state, population, pop_sqmi, sqmi, lat, lng, coordinate, timezone) FROM stdin;
var copyRE = regexp.MustCompile(`(?i)^\s*COPY\s+(.*) \((.*)\) FROM stdin;$`)

func (m *Migrator) applyMigrationPgx(ctx context.Context, conn *sql.Conn, mig *Migration) error {
	return conn.Raw(func(driverConn interface{}) error {
		cx, ok := driverConn.(*stdlib.Conn)
		if !ok {
			return fmt.Errorf("expected *pgx.Conn, got %T", driverConn)
		}

		tx, err := cx.Conn().Begin(ctx)
		if err != nil {
			return err
		}
		if err := m.lockPgx(ctx, tx, mig.Version); err != nil {
			return err
		}
		body, err := mig.body(ctx)
		if err != nil {
			return err
		}
		defer body.Close()
		s := bufio.NewScanner(body)
		var query []string
		var lineNo int
		for s.Scan() {
			lineNo++
			line := s.Text()
			if m := copyRE.FindStringSubmatch(line); m != nil {
				if len(query) > 0 {
					if _, err := tx.Exec(ctx, strings.Join(query, "\n")); err != nil {
						return fmt.Errorf("%s, line %d: %w", mig.Display(), lineNo, err)
					}
					query = query[:0]
				}

				/* Do COPY */
				table := tableName(m[1])
				rawColumns := strings.Split(m[2], ",")
				columns := make([]string, len(rawColumns))
				for i, c := range rawColumns {
					columns[i] = pgUnquote(strings.TrimSpace(c))
				}
				source := &copyRows{Scanner: s, lineNo: &lineNo}
				if _, err := tx.CopyFrom(ctx, table, columns, source); err != nil {
					return fmt.Errorf("%s, line %d: %w", mig.Display(), lineNo, err)
				}
				continue
			}

			query = append(query, line)
		}
		if len(query) > 0 {
			if _, err := tx.Exec(ctx, strings.Join(query, "\n")); err != nil {
				return fmt.Errorf("%s, line %d: %w", mig.Display(), lineNo, err)
			}
		}
		return tx.Commit(ctx)
	})
}

func pgUnquote(identifier string) string {
	if identifier == "" || identifier[0] != '"' {
		return identifier
	}
	// Remove enclosing quotes
	identifier = identifier[1 : len(identifier)-1]
	// Unescape double quotes
	return strings.ReplaceAll(identifier, `""`, `"`)
}

func tableName(table string) pgx.Identifier {
	parts := make([]string, 0, 2)
	part := make([]byte, 0, len(table))
	quoted := false
	for i := 0; i < len(table); i++ {
		switch table[i] {
		case '"':
			// Handle double quotes
			if quoted && len(table) > i+1 && table[i+1] == '"' {
				part = append(part, '"')
				i++
				continue
			}

			// Disable quoting, only at the end of a part
			if quoted && (i+1 == len(table) || table[i+1] == '.') {
				quoted = false
				continue
			}

			// Enable quoting, only at the beginning of a part
			if !quoted && len(part) == 0 {
				quoted = true
				continue
			}

		case '.':
			if !quoted {
				parts = append(parts, string(part))
				part = part[:0]
				continue
			}
		}
		part = append(part, table[i])
	}
	if len(part) > 0 {
		parts = append(parts, string(part))
	}
	return pgx.Identifier(parts)
}

type copyRows struct {
	*bufio.Scanner
	lineNo *int
	line   []byte
}

var _ pgx.CopyFromSource = (*copyRows)(nil)

func (r *copyRows) Next() bool {
	if !r.Scanner.Scan() {
		return false
	}
	*r.lineNo++
	r.line = r.Scanner.Bytes()
	return !bytes.Equal(r.line, []byte(`\.`))
}

func (r *copyRows) Values() ([]any, error) {
	values := bytes.Split(r.line, []byte{'\t'})
	result := make([]any, len(values))
	for i, v := range values {
		if bytes.Equal(v, []byte(`\N`)) {
			result[i] = nil
			continue
		}
		result[i] = string(v)
	}
	return result, nil
}

func (r *copyRows) Err() error {
	return r.Scanner.Err()
}

func (m *Migrator) lockPgx(ctx context.Context, conn pgx.Tx, id uint) error {
	_, err := conn.Exec(ctx, `INSERT INTO `+m.stateTable+` (version) VALUES ($1)`, id)
	return decorateConcurrentMigrationError(err)
}
