package burrow_test

import (
	"context"
	"database/sql"
	"os"
	"testing"
	"time"

	_ "github.com/go-sql-driver/mysql" // MySQL driver
	_ "github.com/jackc/pgx/v5/stdlib" // PostgreSQL driver
	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/modules/mysql"
	"github.com/testcontainers/testcontainers-go/modules/postgres"
	"github.com/testcontainers/testcontainers-go/wait"
)

func startMySql(t *testing.T) *sql.DB {
	t.Helper()
	if os.Getenv("USE_TESTCONTAINERS") == "" {
		t.Skip("USE_TESTCONTAINERS not set; Skipping test that requires testcontainers")
	}

	ctx := context.Background()
	mysqlContainer, err := mysql.Run(ctx, "mysql:8",
		testcontainers.CustomizeRequest(testcontainers.GenericContainerRequest{
			ContainerRequest: testcontainers.ContainerRequest{
				Name: "burrow-mysql-testcontainer",
			},
			Reuse: true,
		}),
		mysql.WithDatabase("burrow"),
		mysql.WithUsername("burrow"),
		mysql.WithPassword("burrow"),
	)
	if err != nil {
		t.Fatal(err)
	}
	containerDSN, err := mysqlContainer.ConnectionString(ctx)
	if err != nil {
		t.Fatal(err)
	}

	db, err := sql.Open("mysql", containerDSN+"?parseTime=true&multiStatements=true")
	if err != nil {
		t.Fatal(err)
	}

	return db
}

func startPostgres(t *testing.T) *sql.DB {
	t.Helper()
	if os.Getenv("USE_TESTCONTAINERS") == "" {
		t.Skip("USE_TESTCONTAINERS not set; Skipping test that requires testcontainers")
	}

	ctx := context.Background()
	postgresContainer, err := postgres.Run(ctx, "postgres:15",
		postgres.WithDatabase("burrow"),
		postgres.WithUsername("burrow"),
		postgres.WithPassword("burrow"),
		testcontainers.WithWaitStrategy(
			wait.ForLog("database system is ready to accept connections").
				WithOccurrence(2).
				WithStartupTimeout(5*time.Second)),
	)
	if err != nil {
		t.Fatal(err)
	}

	containerDSN, err := postgresContainer.ConnectionString(ctx)
	if err != nil {
		t.Fatal(err)
	}

	db, err := sql.Open("pgx", containerDSN)
	if err != nil {
		t.Fatal(err)
	}

	return db
}
