package burrow_test

import (
	"bytes"
	"context"
	"database/sql"
	"errors"
	"fmt"
	"io"
	"log/slog"
	"strings"
	"sync"
	"sync/atomic"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	_ "github.com/mattn/go-sqlite3" //  SQLite driver

	"gitlab.com/flimzy/burrow"
)

func TestMigrate_no_migrations(t *testing.T) {
	t.Parallel()
	ctx := context.Background()
	db, err := sql.Open("sqlite3", "file:no_migrations?mode=memory&cache=shared")
	if err != nil {
		t.Fatal(err)
	}
	mig, err := burrow.New(db, burrow.WithLogger(slog.New(slog.NewTextHandler(io.Discard, nil))))
	if err != nil {
		t.Fatal(err)
	}
	mig.AddSource(burrow.MemorySource{})
	n, err := mig.Migrate(ctx)
	if err != nil {
		t.Fatal(err)
	}
	if n != 0 {
		t.Errorf("Expected 0 migrations, got %d", n)
	}
}

func TestMigrate(t *testing.T) {
	t.Parallel()
	ctx := context.Background()
	db, err := sql.Open("sqlite3", "file:migrate?mode=memory&cache=shared")
	if err != nil {
		t.Fatal(err)
	}
	mig, err := burrow.New(db, burrow.WithLogger(slog.New(slog.NewTextHandler(io.Discard, nil))))
	if err != nil {
		t.Fatal(err)
	}
	mig.AddSource(burrow.MemorySource{
		1: "CREATE TABLE foo (id INTEGER PRIMARY KEY, name TEXT)",
	})
	n, err := mig.Migrate(ctx)
	if err != nil {
		t.Fatal(err)
	}
	if n != 1 {
		t.Errorf("Expected 1 migration, got %d", n)
	}

	// Success?
	_, err = db.Exec(`SELECT COUNT(*) FROM foo`)
	if err != nil {
		t.Fatalf("query against table foo failed: %s", err)
	}
}

func TestMinVer(t *testing.T) {
	t.Parallel()
	ctx := context.Background()
	db, err := sql.Open("sqlite3", "file:minver?mode=memory&cache=shared")
	if err != nil {
		t.Fatal(err)
	}
	logs, logger := newTestLogger(t)
	mig, err := burrow.New(db, burrow.WithLogger(logger))
	if err != nil {
		t.Fatal(err)
	}
	mig.AddSource(burrow.MemorySource{
		1: "CREATE TABLE foo (id INTEGER PRIMARY KEY, name TEXT)",
		2: "CREATE TABLE foo (id INTEGER PRIMARY KEY, name TEXT); INSERT INTO foo (name) VALUES ('foo')",
	})
	mig.SetMinVer(2)
	n, err := mig.Migrate(ctx)
	if err != nil {
		t.Fatal(err)
	}
	if n != 1 {
		t.Errorf("Expected 1 migration, got %d", n)
	}

	// Success?
	_, err = db.Exec(`SELECT COUNT(*) FROM foo`)
	if err != nil {
		t.Fatalf("query against table foo failed: %s", err)
	}
	wantLogs := []string{
		`level=INFO msg="Running migrations" next_version=2 migrations_to_apply=1`,
		`level=INFO msg="migration applied successfully" version=2`,
	}
	if d := cmp.Diff(wantLogs, logs.Logs()); d != "" {
		t.Errorf("Unexpected logs (-want +got):\n%s", d)
	}
}

func TestMigrate_concurrent(t *testing.T) {
	t.Parallel()
	source := burrow.MemorySource{
		1: "UPDATE concurrent SET i=1 WHERE i=0",
		2: "UPDATE concurrent SET i=2 WHERE i=1",
		3: "UPDATE concurrent SET i=3 WHERE i=2",
		4: "UPDATE concurrent SET i=4 WHERE i=3",
		5: "UPDATE concurrent SET i=5 WHERE i=4",
		6: "UPDATE concurrent SET i=6 WHERE i=5",
		7: "UPDATE concurrent SET i=7 WHERE i=6",
		8: "UPDATE concurrent SET i=8 WHERE i=7",
		9: "UPDATE concurrent SET i=9 WHERE i=8",
	}
	const (
		wantMigrationCount = 9
		wantFinalValue     = 9
	)

	ctx := context.Background()
	db := startMySql(t)
	t.Cleanup(func() {
		_ = db.Close()
	})
	if _, err := db.Exec(`CREATE TABLE concurrent (i INTEGER)`); err != nil {
		t.Fatal(err)
	}
	if _, err := db.Exec(`INSERT INTO concurrent (i) VALUES (0)`); err != nil {
		t.Fatal(err)
	}

	const concurrentMigrations = 10
	total := atomic.Int32{}
	wg := sync.WaitGroup{}
	wg.Add(concurrentMigrations)

	for i := 0; i < concurrentMigrations; i++ {
		go func(i int) {
			defer wg.Done()
			mig, err := burrow.New(db,
				burrow.DialectMySQL,
				burrow.WithStateTable("migrations_concurrent"),
				burrow.WithLogger(slog.New(slog.NewTextHandler(io.Discard, nil))))
			if err != nil {
				panic(err)
			}
			mig.AddSource(source)
			n, err := mig.Migrate(ctx)
			if err != nil {
				panic(fmt.Sprintf("%d: %s", i, err))
			}
			total.Add(int32(n))
		}(i)
	}
	wg.Wait()

	if total.Load() != wantMigrationCount {
		t.Errorf("Expected %d migrations, got %d", wantMigrationCount, total.Load())
	}
	var actualFinalValue int
	if err := db.QueryRow(`SELECT i FROM concurrent`).Scan(&actualFinalValue); err != nil {
		t.Fatal(err)
	}
	if actualFinalValue != wantFinalValue {
		t.Errorf("Expected final value %d, got %d", wantFinalValue, actualFinalValue)
	}
}

type testLogger struct {
	buf *bytes.Buffer
}

func (t *testLogger) Logs() []string {
	return strings.Split(strings.TrimSpace(t.buf.String()), "\n")
}

func newTestLogger(t *testing.T) (*testLogger, *slog.Logger) {
	t.Helper()
	b := &bytes.Buffer{}
	l := slog.New(slog.NewTextHandler(b, &slog.HandlerOptions{
		ReplaceAttr: func(_ []string, a slog.Attr) slog.Attr {
			if a.Key == "time" {
				return slog.Attr{}
			}
			return a
		},
	}))
	return &testLogger{b}, l
}

func TestMigrate_skipped_migrations(t *testing.T) {
	t.Parallel()
	ctx := context.Background()
	db, err := sql.Open("sqlite3", "file:skipped_migrations?mode=memory&cache=shared")
	if err != nil {
		t.Fatal(err)
	}
	{
		// Init the migrations table
		mig, err := burrow.New(db, burrow.WithLogger(slog.New(slog.NewTextHandler(io.Discard, nil))))
		if err != nil {
			t.Fatal(err)
		}
		if _, err := mig.Migrate(ctx); err != nil {
			t.Fatal(err)
		}
	}
	if _, err := db.Exec(fmt.Sprintf(`INSERT INTO %s (version) VALUES (2)`, burrow.DefaultStateTable)); err != nil {
		t.Fatal(err)
	}
	mig, err := burrow.New(db, burrow.WithLogger(slog.New(slog.NewTextHandler(io.Discard, nil))))
	if err != nil {
		t.Fatal(err)
	}
	mig.AddSource(burrow.MemorySource{
		1: "CREATE TABLE foo (id INTEGER PRIMARY KEY, name TEXT)",
		2: "CREATE TABLE bar (id INTEGER PRIMARY KEY, name TEXT)",
		3: "CREATE TABLE baz (id INTEGER PRIMARY KEY, name TEXT)",
	})
	n, err := mig.Migrate(ctx)
	if err != nil {
		t.Fatal(err)
	}
	if n != 1 {
		t.Errorf("Expected 1 migration, got %d", n)
	}

	// Success?
	_, err = db.Exec(`SELECT COUNT(*) FROM baz`)
	if err != nil {
		t.Fatalf("query against table foo failed: %s", err)
	}
	_, err = db.Exec(`SELECT COUNT(*) FROM foo`)
	if err == nil || !strings.Contains(err.Error(), "no such table") {
		t.Errorf("Unexpected error: %v", err)
	}
}

func TestMigrate_manual_rollback(t *testing.T) {
	t.Parallel()
	source := burrow.MemorySource{
		1: "UPDATE rollback SET i=1 WHERE i=0",
		2: "CREATE TABLE rollback (i INTEGER); UPDATE rollback SET i=2 WHERE i=1;",
		3: "UPDATE rollback SET i=3 WHERE i=2",
	}
	const (
		wantFinalValue = 1
		wantCurVer     = 1
	)

	ctx := context.Background()
	db := startMySql(t)
	t.Cleanup(func() {
		_ = db.Close()
	})
	if _, err := db.Exec(`CREATE TABLE rollback (i INTEGER)`); err != nil {
		t.Fatal(err)
	}
	if _, err := db.Exec(`INSERT INTO rollback (i) VALUES (0)`); err != nil {
		t.Fatal(err)
	}

	mig, err := burrow.New(db,
		burrow.DialectMySQL,
		burrow.WithStateTable("migrations_rollback"),
		burrow.WithLogger(slog.New(slog.NewTextHandler(io.Discard, nil))),
	)
	if err != nil {
		t.Fatal(err)
	}
	mig.AddSource(source)
	_, err = mig.Migrate(ctx)
	if err == nil || !strings.Contains(err.Error(), "already exists") {
		t.Fatalf("Unexpected migration failure: %s", err)
	}

	var actualFinalValue int
	if err := db.QueryRow(`SELECT i FROM rollback`).Scan(&actualFinalValue); err != nil {
		t.Fatal(err)
	}
	if actualFinalValue != wantFinalValue {
		t.Errorf("Expected final value %d, got %d", wantFinalValue, actualFinalValue)
	}
	var version int64
	err = db.QueryRow(`SELECT MAX(version) FROM migrations_rollback`).Scan(&version)
	if err != nil {
		t.Fatal(err)
	}
	if version != wantCurVer {
		t.Errorf("Expected current version %d, got %d", wantCurVer, version)
	}
}

func Test_postgres_connection(t *testing.T) {
	t.Parallel()
	ctx := context.Background()
	db := startPostgres(t)
	t.Cleanup(func() {
		_ = db.Close()
	})

	mig, err := burrow.New(db,
		burrow.DialectPostgres,
		burrow.WithStateTable("migrations_pg"),
		burrow.WithLogger(slog.New(slog.NewTextHandler(io.Discard, nil))),
	)
	if err != nil {
		t.Fatal(err)
	}
	mig.AddSource(burrow.MemorySource{})
	_, err = mig.Migrate(ctx)
	if err != nil {
		t.Fatal(err)
	}
}

func TestMigrate_pgx_COPY(t *testing.T) {
	t.Parallel()
	ctx := context.Background()
	db := startPostgres(t)
	t.Cleanup(func() {
		_ = db.Close()
	})
	mig, err := burrow.New(db, burrow.DialectPgx, burrow.WithLogger(slog.New(slog.NewTextHandler(io.Discard, nil))))
	if err != nil {
		t.Fatal(err)
	}
	mig.AddSource(burrow.MemorySource{
		1: "CREATE TABLE foo (id INTEGER PRIMARY KEY, name TEXT)",
		2: `INSERT INTO foo (id, name) VALUES (1, 'foo');
COPY "foo" (id, "name") FROM stdin;
2	bar
3	baz
\.

INSERT INTO foo (id, name) VALUES (4, 'qux');

COPY "public"."foo" ("id","name") FROM stdin;
6	bar
\.

COPY "public".foo ("id","name") FROM stdin;
7	baz
\.
`,
	})
	n, err := mig.Migrate(ctx)
	if err != nil {
		t.Fatal(err)
	}
	const want = 2
	if n != want {
		t.Errorf("Expected %d migration, got %d", want, n)
	}

	// Success?
	_, err = db.Exec(`SELECT COUNT(*) FROM foo`)
	if err != nil {
		t.Fatalf("query against table foo failed: %s", err)
	}
}

func TestMigrate_pgx_COPY_error_with_line_number(t *testing.T) {
	t.Parallel()
	ctx := context.Background()
	db := startPostgres(t)
	t.Cleanup(func() {
		_ = db.Close()
	})
	mig, err := burrow.New(db, burrow.DialectPgx, burrow.WithLogger(slog.New(slog.NewTextHandler(io.Discard, nil))))
	if err != nil {
		t.Fatal(err)
	}
	mig.AddSource(burrow.MemorySource{
		1: "CREATE TABLE foo (id INTEGER PRIMARY KEY, name TEXT)",
		2: `INSERT INTO foo (id, name) VALUES (1, 'foo');
COPY "foo" (id, "name") FROM stdin;
oink

`,
	})

	const wantErr = `2, line 3: ERROR: COPY from stdin failed: expected 2 values, got 1 values (SQLSTATE 57014)`
	_, err = mig.Migrate(ctx)
	if err == nil {
		t.Fatal("Expected an error, got none")
	}
	if wantErr != err.Error() {
		t.Fatalf("Unexpected error: %s", err)
	}
}

type execSource struct{}

func (execSource) Index(context.Context) ([]uint, error) {
	return []uint{1}, nil
}

func (execSource) Migration(context.Context, uint) (*burrow.Migration, error) {
	return &burrow.Migration{
		Version: 1,
		Run: func(context.Context, *sql.Tx) error {
			return errors.New("Run failure")
		},
	}, nil
}

func TestMigrate_Run(t *testing.T) {
	t.Parallel()
	ctx := context.Background()
	db, err := sql.Open("sqlite3", "file:migrate_run?mode=memory&cache=shared")
	if err != nil {
		t.Fatal(err)
	}
	mig, err := burrow.New(db, burrow.WithLogger(slog.New(slog.NewTextHandler(io.Discard, nil))))
	if err != nil {
		t.Fatal(err)
	}
	mig.AddSource(execSource{})
	_, err = mig.Migrate(ctx)
	if err == nil || !strings.Contains(err.Error(), "Run failure") {
		t.Fatalf("Unexpected error: %v", err)
	}
}

type testSource []burrow.Migration

func (t testSource) Index(context.Context) ([]uint, error) {
	ret := make([]uint, 0, len(t))
	for _, m := range t {
		ret = append(ret, m.Version)
	}
	return ret, nil
}

func (t testSource) Migration(_ context.Context, id uint) (*burrow.Migration, error) {
	for _, m := range t {
		if m.Version == id {
			return &m, nil
		}
	}
	return nil, errors.New("migration not found")
}

func TestMigrate_PreRun(t *testing.T) {
	t.Parallel()
	ctx := context.Background()
	db, err := sql.Open("sqlite3", "file:migrate_prerun?mode=memory&cache=shared")
	if err != nil {
		t.Fatal(err)
	}
	mig, err := burrow.New(db, burrow.WithLogger(slog.New(slog.NewTextHandler(io.Discard, nil))))
	if err != nil {
		t.Fatal(err)
	}
	var preRunCount atomic.Int32
	const wantPreRunCount = 2
	mig.AddSource(testSource{
		burrow.Migration{
			Version: 1,
			PreRun: func(context.Context) error {
				time.Sleep(100 * time.Millisecond)
				preRunCount.Add(1)
				return nil
			},
			GetBody: func(context.Context) (io.Reader, error) {
				return strings.NewReader("CREATE TABLE foo (id INTEGER PRIMARY KEY, name TEXT)"), nil
			},
		},
		burrow.Migration{
			Version: 2,
			PreRun: func(context.Context) error {
				time.Sleep(100 * time.Millisecond)
				preRunCount.Add(1)
				return nil
			},
			Run: func(_ context.Context, _ *sql.Tx) error {
				if preRunCount.Load() != wantPreRunCount {
					return fmt.Errorf("Expected %d pre-run calls, got %d", wantPreRunCount, preRunCount.Load())
				}
				return nil
			},
		},
		burrow.Migration{
			Version: 3,
			PreRun: func(context.Context) error {
				return errors.New("PreRun failure")
			},
		},
	})
	start := time.Now()
	_, err = mig.Migrate(ctx)
	if elapsed := time.Since(start); elapsed < 100*time.Millisecond || elapsed > 110*time.Millisecond {
		t.Errorf("Expected pre-run functions to take 100-110ms, took %s", time.Since(start))
	}
	if err == nil || !strings.Contains(err.Error(), "PreRun failure") {
		t.Fatalf("Unexpected error: %v", err)
	}
}
