package burrow_test

import (
	"bytes"
	"context"
	"database/sql"
	"io"
	"log/slog"
	"os"
	"testing"

	"gitlab.com/flimzy/testy"

	"gitlab.com/flimzy/burrow"
)

func TestMigrate_executable_scripts(t *testing.T) {
	type test struct {
		db      *sql.DB
		source  burrow.Source
		wantEnv map[string]string
	}

	tests := testy.NewTable()
	tests.Add("sqlite, memory mode, filesystem source", func(t *testing.T) any {
		db, err := sql.Open("sqlite3", "file:executable_scripts?mode=memory&cache=shared")
		if err != nil {
			t.Fatal(err)
		}
		return test{
			db:      db,
			wantEnv: map[string]string{"GREETING": "Hello"},
		}
	})
	tests.Add("sqlite, memory mode, fs.FS source", func(t *testing.T) any {
		db, err := sql.Open("sqlite3", "file:executable_scripts_fs?mode=memory&cache=shared")
		if err != nil {
			t.Fatal(err)
		}
		fs := os.DirFS("testdata/executable")
		source, err := burrow.FSSource(fs, burrow.Executable(`\.sh$`))
		if err != nil {
			t.Fatal(err)
		}

		return test{
			db:      db,
			source:  source,
			wantEnv: map[string]string{"GREETING": "Hello"},
		}
	})

	tests.Run(t, func(t *testing.T, tt test) {
		source := tt.source
		if source == nil {
			var err error
			source, err = burrow.FileSource("testdata/executable", burrow.Executable(`\.sh$`))
			if err != nil {
				t.Fatal(err)
			}
		}
		ctx := context.Background()
		mig, err := burrow.New(tt.db, burrow.WithLogger(slog.New(slog.NewTextHandler(io.Discard, nil))))
		if err != nil {
			t.Fatal(err)
		}

		mig.AddSource(source)

		var n int
		stdout, _ := testy.RedirIO(nil, func() {
			n, err = mig.Migrate(ctx)
		})
		if err != nil {
			t.Fatal(err)
		}
		if n != 2 {
			t.Errorf("Expected 2 migrations, got %d", n)
		}

		compareEnv(t, stdout, tt.wantEnv)
	})
}

func compareEnv(t *testing.T, stdout io.Reader, want map[string]string) {
	t.Helper()
	fullEnv, err := io.ReadAll(stdout)
	if err != nil {
		t.Fatal(err)
	}
	env := make(map[string]string)
	for _, line := range bytes.Split(fullEnv, []byte("\n")) {
		if len(line) == 0 {
			continue
		}
		parts := bytes.SplitN(line, []byte("="), 2)
		if len(parts) == 2 {
			env[string(parts[0])] = string(parts[1])
		}
	}

	for key, wantVal := range want {
		if gotVal := env[key]; gotVal != wantVal {
			t.Errorf("Expected env var %s=%q, got %q", key, wantVal, gotVal)
		}
	}
}
