# burrow

Go database migration tool.

When gophers migrate, they burrow!

## Design goals

- **Simple to use.**
- **No downgrade scripts.** — Making every migration bi-directional is an immense amount of work that I've literally never seen done properly. And what's worse, it's bad practice. Just make sure your upgrades are non-destructive, and you can avoid this problem entirely.
- **Database agnostic** — Should work with any `database/sql` driver.
- **Allow reading from multiple migration directories.** — Sometimes you want to interweave migrations in some environments (notably test environments).
