package burrow

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"slices"
	"strconv"
	"strings"
)

type Migration struct {
	Version uint
	// Name is a descriptive name, such as full path to the migration, used for
	// debugging/informational purposes.
	Name string
	// Body is expected to return the SQL to execute for the migration.
	Body io.Reader

	// PreRun is an optional function to run before executing [Body] or [Run].
	// It is primarily intended for migrations that need to download large data
	// sets (i.e. from a cloud storage bucket) or perform other expensive
	// operations before running the migration. By putting such work in PreRun,
	// it may be run concurrently with other migrations or other PreRun
	// functions. If PreRun returns an error, the migration itself will fail.
	PreRun func(context.Context) error

	// GetBody is an optional function to get the body of the migration. If set,
	// [Body] is ignored.
	GetBody func(context.Context) (io.Reader, error)

	// Run is an optional function to run instead of executing [Body] as SQL. If
	// set, [Body] is ignored.
	Run func(context.Context, *sql.Tx) error
}

// body returns the migration body, either by calling [Migration.GetBody], or
// returning using [Migration.Body] directly, upgraded or converted to an
// [io.ReadCloser] for caller convenience.
func (m *Migration) body(ctx context.Context) (io.ReadCloser, error) {
	body := m.Body
	if m.GetBody != nil {
		b, err := m.GetBody(ctx)
		switch err {
		case nil:
			body = b
		case ErrUnimplemented:
			// Use m.Body as-is
		default:
			return nil, err
		}
	}
	if rc, ok := body.(io.ReadCloser); ok {
		return rc, nil
	}
	return io.NopCloser(body), nil
}

func (m *Migration) Display() string {
	if m.Name != "" {
		return fmt.Sprintf("%d: %s", m.Version, m.Name)
	}
	return strconv.Itoa(int(m.Version))
}

// Source is the interface for a migration source.
type Source interface {
	// Index returns a list of all available migration versions.
	Index(context.Context) ([]uint, error)
	// Migration is expected to return the numbered migration.
	Migration(_ context.Context, id uint) (*Migration, error)
}

type particulars struct {
	dialect dialect
	db      *sql.DB
}

// If a source implements this interface, it will be passed some specific
// particulars about the database in use.
type sourceParticulars interface {
	setParticulars(context.Context, *particulars) error
}

// MemorySource is a Source implemented by an in-memory map
type MemorySource map[uint]string

var _ Source = MemorySource{}

// Index returns the list of migration IDs in the map.
func (s MemorySource) Index(context.Context) ([]uint, error) {
	ret := make([]uint, 0, len(s))
	for k := range s {
		ret = append(ret, k)
	}
	return ret, nil
}

// Migration returns the migration with the given ID, or an error if not found.
func (s MemorySource) Migration(_ context.Context, id uint) (*Migration, error) {
	body, ok := s[id]
	if !ok {
		return nil, errors.New("migration not found")
	}
	return &Migration{
		Version: id,
		Body:    strings.NewReader(body),
	}, nil
}

type filesystem interface {
	fs.FS
	// executable returns the path to the executable file with the given name,
	// and a cleanup function.
	executable(string) (string, func(), error)
}

type fsSource struct {
	filesystem
	dialect      dialect
	sqliteDBFile string
	files        map[uint]string
	excludes     []*regexp.Regexp
	executable   []*regexp.Regexp
}

func (s *fsSource) setParticulars(ctx context.Context, p *particulars) error {
	s.dialect = p.dialect
	switch p.dialect {
	case DialectSQLite:
		return p.db.QueryRowContext(ctx, "SELECT file FROM pragma_database_list LIMIT 1").
			Scan(&s.sqliteDBFile)
	}
	return nil
}

// Index returns the list of migration IDs in the filesystem.
func (s *fsSource) Index(context.Context) ([]uint, error) {
	dir, err := fs.ReadDir(s.filesystem, ".")
	if err != nil {
		return nil, err
	}
	ret := make([]uint, 0, len(dir))
	want := append(s.executable, regexp.MustCompile(`\.sql$`))
	for _, f := range dir {
		if f.IsDir() {
			continue
		}
		if slices.ContainsFunc(s.excludes, func(re *regexp.Regexp) bool {
			return re.MatchString(f.Name())
		}) {
			continue
		}
		if !slices.ContainsFunc(want, func(re *regexp.Regexp) bool {
			return re.MatchString(f.Name())
		}) {
			continue
		}

		filename := strings.TrimSuffix(f.Name(), filepath.Ext(f.Name()))
		parts := strings.SplitN(filename, "_", 2)
		ver, err := strconv.ParseUint(parts[0], 10, 64)
		if err != nil {
			return nil, fmt.Errorf("invalid migration file name: %s", f.Name())
		}

		if file, ok := s.files[uint(ver)]; ok {
			return nil, fmt.Errorf("duplicate migration %d: %s and %s", ver, file, f.Name())
		}
		s.files[uint(ver)] = f.Name()
		ret = append(ret, uint(ver))
	}
	return ret, nil
}

func (s *fsSource) env() []string {
	return nil
}

// Migration returns the migration with the given ID, or an error if not found.
func (s *fsSource) Migration(_ context.Context, id uint) (*Migration, error) {
	mig := &Migration{
		Version: id,
		Name:    s.files[id],
	}
	filename := s.files[id]

	switch {
	case slices.ContainsFunc(s.executable, func(re *regexp.Regexp) bool {
		return re.MatchString(filename)
	}):
		mig.Run = func(ctx context.Context, tx *sql.Tx) error {
			executable, cleanup, err := s.filesystem.executable(filename)
			if err != nil {
				return err
			}
			defer cleanup()
			cmd := exec.CommandContext(ctx, executable)
			cmd.Env = append(os.Environ(), s.env()...)
			cmd.Stdout = os.Stdout
			cmd.Stderr = os.Stderr
			return cmd.Run()
		}
	default:
		var err error
		mig.Body, err = s.filesystem.Open(filename)
		if err != nil {
			return nil, err
		}
	}

	return mig, nil
}

type realFilesystem struct {
	dir string
	fs.FS
}

func (r *realFilesystem) executable(name string) (string, func(), error) {
	return filepath.Join(r.dir, name), func() {}, nil
}

// FileSource returns a Source implemented by a directory of files. By default,
// it only considers files that end in ".sql", and you may optionally exclude
// other filepatterns by passing [Exclude] options.
func FileSource(dir string, options ...Option) (Source, error) {
	fs := &realFilesystem{
		dir: dir,
		FS:  os.DirFS(dir),
	}
	return newFSSource(fs, options...)
}

type virtualFilesystem struct {
	fs.FS
}

func (v *virtualFilesystem) executable(name string) (string, func(), error) {
	tmp, err := os.CreateTemp("", "burrow")
	if err != nil {
		return "", nil, err
	}
	defer tmp.Close()
	f, err := v.FS.Open(name)
	if err != nil {
		return "", nil, err
	}
	defer f.Close()
	if _, err := io.Copy(tmp, f); err != nil {
		return "", nil, err
	}
	// Set the executable bit on the file.
	if err := tmp.Chmod(0o700); err != nil {
		return "", nil, err
	}
	return tmp.Name(), func() {
		_ = os.Remove(tmp.Name())
	}, nil
}

// FSSource returns a Source implemented by a directory of files. See
// [FileSource] for full description of behavior.
func FSSource(fsys fs.FS, options ...Option) (Source, error) {
	return newFSSource(&virtualFilesystem{fsys}, options...)
}

func newFSSource(fsys filesystem, options ...Option) (*fsSource, error) {
	s := &fsSource{
		filesystem: fsys,
		files:      map[uint]string{},
	}
	for _, opt := range options {
		if err := opt.apply(s); err != nil {
			return nil, err
		}
	}
	return s, nil
}
