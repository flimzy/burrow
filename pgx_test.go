package burrow

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/jackc/pgx/v5"
)

func Test_tableName(t *testing.T) {
	tests := []struct {
		input string
		want  pgx.Identifier
	}{
		{
			input: "foo",
			want:  pgx.Identifier{"foo"},
		},
		{
			input: `"foo"`,
			want:  pgx.Identifier{"foo"},
		},
		{
			input: `"foo""bar"`,
			want:  pgx.Identifier{"foo\"bar"},
		},
		{
			input: "foo.bar",
			want:  pgx.Identifier{"foo", "bar"},
		},
		{
			input: `"foo".bar`,
			want:  pgx.Identifier{"foo", "bar"},
		},
		{
			input: `"foo""bar".baz`,
			want:  pgx.Identifier{"foo\"bar", "baz"},
		},
		{
			input: `foo"`,
			want:  pgx.Identifier{"foo\""},
		},
		{
			input: `foo"bar"`,
			want:  pgx.Identifier{"foo\"bar\""},
		},
	}

	for _, tt := range tests {
		t.Run(tt.input, func(t *testing.T) {
			got := tableName(tt.input)
			if d := cmp.Diff(tt.want, got); d != "" {
				t.Errorf("tableName(%q) got diff (-want +got): %s", tt.input, d)
			}
		})
	}
}
