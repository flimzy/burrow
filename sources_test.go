package burrow

import (
	"context"
	"io"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestFileSource_Index(t *testing.T) {
	ctx := context.Background()
	source, err := FileSource("testdata/migrations")
	if err != nil {
		t.Fatal(err)
	}
	wantIndex := []uint{0, 1, 31, 32}
	index, err := source.Index(ctx)
	if err != nil {
		t.Fatal(err)
	}
	if d := cmp.Diff(wantIndex, index); d != "" {
		t.Error(d)
	}
}

func TestFileSource_Index_filtered(t *testing.T) {
	ctx := context.Background()
	source, err := FileSource("testdata/migrations", Exclude(`_test\.sql$`))
	if err != nil {
		t.Fatal(err)
	}
	wantIndex := []uint{0, 1, 31}
	index, err := source.Index(ctx)
	if err != nil {
		t.Fatal(err)
	}
	if d := cmp.Diff(wantIndex, index); d != "" {
		t.Error(d)
	}
}

func TestFileSource_Index_with_extensions(t *testing.T) {
	ctx := context.Background()
	source, err := FileSource("testdata/migrations", Executable(".sh"))
	if err != nil {
		t.Fatal(err)
	}
	wantIndex := []uint{0, 1, 15, 31, 32}
	index, err := source.Index(ctx)
	if err != nil {
		t.Fatal(err)
	}
	if d := cmp.Diff(wantIndex, index); d != "" {
		t.Error(d)
	}
}

func TestFileSource_invalid_exclude(t *testing.T) {
	_, err := FileSource("testdata/migrations", Exclude(`(`))
	if err == nil {
		t.Fatal("expected error, got nil")
	}
}

func TestFileSource_Index_duplicate(t *testing.T) {
	ctx := context.Background()
	source, err := FileSource("testdata/dupe")
	if err != nil {
		t.Fatal(err)
	}
	_, err = source.Index(ctx)
	want := "duplicate migration 0: 0000.sql and 0000_dupe.sql"
	if err.Error() != want {
		t.Errorf("got %q, want %q", err.Error(), want)
	}
}

func TestFileSource_Migration(t *testing.T) {
	ctx := context.Background()
	source, err := FileSource("testdata/migrations")
	if err != nil {
		t.Fatal(err)
	}
	index, err := source.Index(ctx)
	if err != nil {
		t.Fatal(err)
	}
	want := `CREATE TABLE foo;`
	mig, err := source.Migration(ctx, index[0])
	if err != nil {
		t.Fatal(err)
	}
	got, err := io.ReadAll(mig.Body)
	if err != nil {
		t.Fatal(err)
	}
	if string(got) != want {
		t.Errorf("got %q, want %q", got, want)
	}
}

func TestFileSource_Migration_executable(t *testing.T) {
	ctx := context.Background()
	source, err := FileSource("testdata/migrations", Executable(`\.sh$`))
	if err != nil {
		t.Fatal(err)
	}
	const ver = 15
	if _, err := source.Index(ctx); err != nil {
		t.Fatal(err)
	}

	mig, err := source.Migration(ctx, ver)
	if err != nil {
		t.Fatal(err)
	}
	if mig.Run == nil {
		t.Error("expected Run to be non-nil")
	}
}
